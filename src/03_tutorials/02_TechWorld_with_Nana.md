# TechWorld with Nana

> Title: `Kubernetes Tutorial for Beginners [FULL COURSE in 4 Hours]`
>
> Source: [Youtube](<https://www.youtube.com/watch?v=X48VuDVv0do>)

## Chapters

### Course Overview

### What is K8s

### Main K8s Components

---

> `Pods`
>
> _abstraction of containers_
>
> > Pods are the smallest deployable units of computing that you can create and manage in Kubernetes.
>
> - smallest unit of K8s
> - abstraction over container
> - usually 1 application per `Pod`
> - each `Pod` gets its own random IP address

---

> `Services`
>
> _communication_
>
> > An abstract way to expose an application running on a set of Pods as a network service.
>
> - permanent IP address
> - the livecycle of `Pods` and `Services` is NOT connected
> - can be __internal__ or __external__
> - `Pods` communicate with eachother through __services__
>
> They have 2 functionalities:
>
> - permanent IP
> - load balancer

---

> `Ingress`
>
> _route traffic into cluster_
>
> > An API object that manages external access to the services in a cluster, typically HTTP.

---

> `ConfigMaps`
>
> _configuration_
>
> > A ConfigMap is an API object used to store non-confidential data in key-value pairs. Pods can consume ConfigMaps as environment variables, command-line arguments, or as configuration files in a volume.
>
> - external configuration of applications

---

> `Secrets`
>
> _configuration_
>
> > A Secret is an object that contains a small amount of sensitive data such as a password, a token, or a key. Such information might otherwise be put in a Pod specification or in a container image. Using a Secret means that you don't need to include confidential data in your application code.
>
> - used to store secret data
> - base64 encoded
>
> Warning: The built-in security mechanism is __not__ enabled by default!

---

> `Volumes`
>
> _data persistence_
>
> > On-disk files in a container are ephemeral, which presents some problems for non-trivial applications when running in containers. One problem is the loss of files when a container crashes. The kubelet restarts the container but with a clean state. A second problem occurs when sharing files between containers running together in a Pod. The Kubernetes volume abstraction solves both of these problems. Familiarity with Pods is suggested.
>
> - attaches physical __local__ or __remote__ storage to `Pods`
> - storage should be regarded as an __extension__ to the cluster: __k8s doesn't manage data persistance!__

---

> `Deployments`
>
> `Pod` blueprints for __stateless__ applications
>
> > A Deployment provides declarative updates for Pods and ReplicaSets.
> >
> > You describe a desired state in a Deployment, and the Deployment Controller changes the actual state to the desired state at a controlled rate. You can define Deployments to create new ReplicaSets, or to remove existing Deployments and adopt all their resources with new Deployments.
>
> - blueprint for `Pods`
> - abstraction of `Pods`
> - manages _replicas_ of `Pods`
> - __databases__ can't be replicated via `Deployments` because they have __state__

---

> `StatefulSets`
>
> `Pod` blueprints for __stateful__ applications
>
> > StatefulSet is the workload API object used to manage stateful applications.
> >
> > Manages the deployment and scaling of a set of Pods, and provides guarantees about the ordering and uniqueness of these Pods.
> >
> > Like a Deployment, a StatefulSet manages Pods that are based on an identical container spec. Unlike a Deployment, a StatefulSet maintains a sticky identity for each of their Pods. These pods are created from the same spec, but are not interchangeable: each has a persistent identifier that it maintains across any rescheduling.
> >
> > If you want to use storage volumes to provide persistence for your workload, you can use a StatefulSet as part of the solution. Although individual Pods in a StatefulSet are susceptible to failure, the persistent Pod identifiers make it easier to match existing volumes to the new Pods that replace any that have failed.
>
> - specifically for __stateful__ applications like __databases__
> - deploying __databases__ using `StatefulSets` is __not__ easy
>   - therefor __databases__ are often hosted __outside__ of the __k8s cluster__

### K8s Architecture

### Minikube and kubectl - Local Setup

### Main Kubectl Commands - K8s CLI

#### CRUD commands

| Description           | Command                            |
| --------------------- | ---------------------------------- |
| __Create__ deployment | `kubectl create deployment [name]` |
| __Edit__ deployment   | `kubectl edit deployment [name]`   |
| __Delete__ deployment | `kubectl delete deployment [name]` |

#### Status of different k8s components

```sh
kubectl get [all|nodes|pods|services|replicasets|deployments]
```

#### Debugging pods

| Description                  | Command                               |
| ---------------------------- | ------------------------------------- |
| __Log__ to console           | `kubectl logs [pod name]`             |
| Get __Interactive Terminal__ | `kubectl exec -it [pod name] -- bash` |
| Get __info__ about pod       | `kubectl describe pod [pod name]`     |

#### Use configuration file for CRUD

| Description                        | Command                         |
| ---------------------------------- | ------------------------------- |
| __Apply__ a configuration file     | `kubectl apply -f [file name]`  |
| __Delete__ with configuration file | `kubectl delete -f [file name]` |

#### Examples

```sh
kubectl get nodes
kubectl get pods
kubectl get services
kubectl get statefulsets
kubectl get deployments
kubectl get replicasets

kubectl create deployment nginx-depl --image="nginx:latest"

kubectl edit deployment nginx-depl

kubectl logs nginx-depl-SOME_ID

kubectl exec -it nginx-depl-SOME_ID -- /bin/sh

kubectl delete deployment nginx-depl
```

### K8s YAML Configuration File

#### Each configuration file has __3 parts__

| Part            | Description                                                                   |
| --------------- | ----------------------------------------------------------------------------- |
| `status`        | Managed by Kubernetes. Used internally to manage the state and updates to it. |
| `metadata`      | Identification Data for the component.                                        |
| `specification` | Actual configuration for the component.                                       |

```yaml
# Deployment

apiVersion: apps/v1
kind: Deployment
# `metadata`
metadata:
  name: nginx-deployment
  labels: [...]
# `specification`
spec:
  replicas: 2
  selector: [...]
  template: [...]
```

```yaml
# Service

apiVersion: v1
kind: Service
# `metadata`
metadata:
  name: nginx-deployment
# `specification`
spec:
  selector: [...]
  ports: [...]
```

### Demo Project: MongoDB and MongoExpress

### Organizing your components with K8s Namespaces

### K8s Ingress explained

### Helm - Package Manager

### Persisting Data in K8s with Volumes

### Deploying Stateful Apps with StatefulSet

### K8s Services explained
