# Networkchuck

> Title: `you need to learn Kubernetes RIGHT NOW!!`
>
> Source: [Youtube](<https://www.youtube.com/watch?v=7bA0gTroJjw>)

The Video doesn't describe how to create a kubernetes Cluster. I was using Minikube for that.

## First: Start the Kubernetes Cluster

```sh
minikube start
```

## Running a simple pod

```sh
# "run" a pod
kubectl run networkchuckcoffee --image=thenetworkchuck/nccoffee:pourover --port=80

# verify it is running
kubectl get pods
```

## Create a deployment

First up, create a deployment __manifest__ file.

```yaml
# networkchuckcoffee_deployment.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: networkchuckcoffee-deployment
  labels:
    app: nccoffee
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nccoffee
  template:
    metadata:
      labels:
        app: nccoffee
    spec:
      containers:
      - name: nccoffee
        image: thenetworkchuck/nccoffee:pourover
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```

Then load it into kubernetes.

```sh
kubectl apply -f "./networkchuckcoffee_deployment.yaml"
```

## Create a service

The deployment worked but it is not accessible through the browser. To be able to access it a __service__ which exposes it is required.

```yaml
# networkchuckcoffee_service.yaml

apiVersion: v1
kind: Service
metadata:
  name: coffee-service
  labels:
    app: coffee-service
spec:
  type: LoadBalancer
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nccoffee
  sessionAffinity: None
```

And again: load it into kubernetes.

```sh
kubectl apply -f "./networkchuckcoffee_service.yaml"
```

## Accessing the service

Since i was using `minikube` i needed to find the correct exposed address:

```sh
minikube service --all
```

And as a nice-to-have i can view the running services through the kubernetes dashboard through `minikube`.

```sh
minicube dashboard
```
