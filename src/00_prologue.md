# Kubernetes

## Kubernetes Distributions

| Distribution                                     | Description                                     |
| ------------------------------------------------ | ----------------------------------------------- |
| [k8s](<https://kubernetes.io/>)                  | standard kubernetes with all bells and whistles |
| [k3s](<https://rancher.com/products/k3s>)        | rancher minimal kubernetes                      |
| [k3d](<https://k3d.io/>)                         | `k3s` in docker                                 |
| [minikube](<https://minikube.sigs.k8s.io/docs/>) | minimal kubernetes for local `testing`          |

## Kubernetes Dashboards

- [Kubernetes](<https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/>)
- [Kubernetes Lens](<https://k8slens.dev/desktop.html>)
- [ArgoCD](<https://argo-cd.readthedocs.io/en/stable/user-guide/commands/argocd_admin_dashboard/>)
- [Linkerd](<https://linkerd.io/2.11/features/dashboard/#linkerd-dashboard>)
- [Rancher](<https://rancher.com/products/rancher>)
