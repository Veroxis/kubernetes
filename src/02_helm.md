# Helm

## Useful Commands

```sh
helm search <chartname>
helm install <chartname>
```

## Structure

| Location          | Short Description                                                       |
| ----------------- | ----------------------------------------------------------------------- |
| `mathservice/`    | name of the chart                                                       |
| `├── Chart.yaml`  | meta info about the chart (`name`, `version`, `dependencies` etc.)      |
| `├── values.yaml` | values for the template files (default values which can be overwritten) |
| `├── charts/`     | chart dependencies                                                      |
| `└── templates/`  | the actual template files                                               |

Other files could be a readme or a license. They are allowed aswell.
